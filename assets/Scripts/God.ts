import { Consts } from "./Consts"; 

const {
  ccclass,
  property
} = cc._decorator;

@ccclass
export default class God extends cc.Component {
	@property(cc.Prefab) player: cc.Prefab = null;
  @property({ type: [cc.AudioClip] }) audioClips: AudioClip[] = [];

  public wDown: boolean = false;
  public aDown: boolean = false;
  public sDown: boolean = false;
  public dDown: boolean = false;
  public vDown: boolean = false;
  public bDown: boolean = false;
  public nDown: boolean = false;

  public arrowUpDown: boolean = false;
  public arrowLeftDown: boolean = false;
  public arrowDownDown: boolean = false;
  public arrowRightDown: boolean = false;
	public iDown: boolean = false;
	public oDown: boolean = false;
	public pDown: boolean = false;

	public player1: null;
	public player2: null;
	public player1Skin: null;
	public player2Skin: null;
  public prevMap: string = "choose";
	public waitToAddPlayer: string = "map1";
  public transitioning: boolean = false;

  onLoad() {
    cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    cc.director.getCollisionManager().enabled = true;
    /* cc.director.getCollisionManager().enabledDebugDraw = true; */
  }

  start() {
    cc.audioEngine.playMusic(this.audioClips[Consts.AUDIO_BGM_MENU], true);
    cc.game.addPersistRootNode(this.node);
  }

  update(dt) {
    if (this.prevMap == "choose" && cc.director.getScene().name != "choose") {
      this.prevMap = cc.director.getScene().name;
      cc.audioEngine.stopMusic();
      cc.audioEngine.playMusic(this.audioClips[Consts.AUDIO_BGM_MAP], true);
    }
		if (this.waitToAddPlayer == "map1" && cc.director.getScene().name == "map1") {
      this.transitioning = false;
      this.waitToAddPlayer = "map2";
			this.player1.parent = cc.director.getScene();
			this.player1.setPosition(200, 133);
			this.player2.parent = cc.director.getScene();
			this.player2.setPosition(800, 133);
		}
		if (this.waitToAddPlayer == "map2" && cc.director.getScene().name == "map2") {
      this.transitioning = false;
      this.waitToAddPlayer = "map3";
			this.player1.parent = cc.director.getScene();
			this.player1.setPosition(253, 210);
			this.player2.parent = cc.director.getScene();
			this.player2.setPosition(835, 304);
		}
		if (this.waitToAddPlayer == "map3" && cc.director.getScene().name == "map3") {
      this.transitioning = false;
      this.waitToAddPlayer = "map1";
			this.player1.parent = cc.director.getScene();
			this.player1.setPosition(190, 160);
			this.player2.parent = cc.director.getScene();
			this.player2.setPosition(400, 160);
		}
	}

  onKeyDown(event) {
    switch (event.keyCode) {
			case cc.macro.KEY.w: this.wDown = true; break;
			case cc.macro.KEY.a: this.aDown = true; break;
			case cc.macro.KEY.s: this.sDown = true; break;
			case cc.macro.KEY.d: this.dDown = true; break;
			case cc.macro.KEY.v: this.vDown = true; break;
			case cc.macro.KEY.b: this.bDown = true; break;
			case cc.macro.KEY.n: this.nDown = true; break;

			case cc.macro.KEY.up: this.arrowUpDown = true; break;
			case cc.macro.KEY.left: this.arrowLeftDown = true; break;
			case cc.macro.KEY.down: this.arrowDownDown = true; break;
			case cc.macro.KEY.right: this.arrowRightDown = true; break;
			case cc.macro.KEY.i: this.iDown = true; break;
			case cc.macro.KEY.o: this.oDown = true; break;
			case cc.macro.KEY.p: this.pDown = true; break;
		}
	}

	onKeyUp(event) {
		switch (event.keyCode) {
			case cc.macro.KEY.enter: this.enter(); break;

			case cc.macro.KEY.w: this.wDown = false; break;
			case cc.macro.KEY.a: this.aDown = false; this.move(-1, 1); break;
			case cc.macro.KEY.s: this.sDown = false; break;
			case cc.macro.KEY.d: this.dDown = false; this.move(+1, 1); break;
			case cc.macro.KEY.v: this.vDown = false; break;
			case cc.macro.KEY.b: this.bDown = false; break;
			case cc.macro.KEY.n: this.nDown = false; break;

			case cc.macro.KEY.up: this.arrowUpDown = false; break;
			case cc.macro.KEY.left: this.arrowLeftDown = false; this.move(-1, 2);break;
			case cc.macro.KEY.down: this.arrowDownDown = false; break;
			case cc.macro.KEY.right: this.arrowRightDown = false; this.move(+1, 2); break;
			case cc.macro.KEY.i: this.iDown = false; break;
			case cc.macro.KEY.o: this.oDown = false; break;
			case cc.macro.KEY.p: this.pDown = false; break;
		}
	}

	move(dir, player) {
		let cursor = cc.find(player == 1 ? "red_p1" : "green_p2");
		if (cursor) {
			cursor.x += dir * 192;
			if (cursor.x < 192) cursor.x = 192 * 4;
			if (cursor.x > 192 * 4) cursor.x = 192;
		}
	}

	enter() {
		if (cc.director.getScene().name == "choose") {
			let skin = ["color presets/black and white", "color presets/inverted", "blue", "purple"];
      this.player1Skin = skin[cc.find("red_p1").x / 192 - 1];
      this.player2Skin = skin[cc.find("green_p2").x / 192 - 1];
      this.nextMap();
		}
	}

  nextMap(freezeTime = 0) {
    this.transitioning = true;
		this.schedule(() {
      this.player1 = cc.instantiate(this.player);
      this.player2 = cc.instantiate(this.player);
      this.player1.getComponent(sp.Skeleton).defaultSkin = this.player1Skin;
      this.player1.getComponent("PlayerController").isPlayer1 = true;
      this.player2.getComponent(sp.Skeleton).defaultSkin = this.player2Skin;
      this.player2.scaleX = -0.08;
      this.player2.getComponent("PlayerController").isPlayer1 = false;
      cc.director.loadScene(this.waitToAddPlayer); 
    }, 0, 0, freezeTime);
    // interval, repeat, delay
  }
}

