import { Consts } from "./Consts"; 

const {
  ccclass,
  property
} = cc._decorator;

@ccclass
export default class GiftController extends cc.Component {
	public god: cc.Component = null;
	public vanishing: boolean = false;

  onLoad() {}

  start() {
		this.god = cc.find("God").getComponent("God");
		setTimeout(function () {
      if (!this.vanishing && this.node) this.node.destroy();
    }.bind(this), 5000);
	}

  update (dt) {}

	onBeginContact(contact, self, other) {
		if (!this.vanishing && other.node.name == "stickman fighter") {
			this.vanishing = true;
			contact.disabled = true;
			other.getComponent("PlayerController").mode = 2 + Math.floor(Math.random() * 4);
      switch (other.getComponent("PlayerController").mode) {
        case Consts.MODE_SWORD:
          cc.audioEngine.playEffect(this.god.audioClips[Consts.AUDIO_EFFECT_PICKUP_SWORD]);
          break;
        case Consts.MODE_PISTOL:
          cc.audioEngine.playEffect(this.god.audioClips[Consts.AUDIO_EFFECT_PICKUP_PISTOL]);
          break;
        case Consts.MODE_UZI:
        case Consts.MODE_RIFLE:
          cc.audioEngine.playEffect(this.god.audioClips[Consts.AUDIO_EFFECT_PICKUP_MGUNS]);
          break;
      }
			this.node.destroy();
		}
	}
}
