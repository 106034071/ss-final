export class Consts {
	public static readonly TAG_GROUND = 1;
	public static readonly TAG_BULLET = 10;

	public static readonly MODES: String[] = ["0", "1_", "2_Sword", "3_Pistol", "4_Uzi", "5_Rifle"];
  public static readonly MODE_FIST = 1;
  public static readonly MODE_SWORD = 2;
  public static readonly MODE_PISTOL = 3;
  public static readonly MODE_UZI = 4;
  public static readonly MODE_RIFLE = 5;
  public static readonly MODE_BULLET_SPEED = [0, 0, 0, 500, 800, 1200];
  public static readonly MODE_BULLET_CD = [0, 0, 0, 0.5, 0.25, 0.1];

  public static readonly AUDIO_BGM_MENU = 0;
  public static readonly AUDIO_BGM_MAP = 1;
  public static readonly AUDIO_EFFECT_LAND = 2;
  public static readonly AUDIO_EFFECT_PICKUP_PISTOL = 3;
  public static readonly AUDIO_EFFECT_PICKUP_MGUNS = 4;
  public static readonly AUDIO_EFFECT_PICKUP_SWORD = 5;
  public static readonly AUDIO_EFFECT_ATTACK_PISTOL = 6;
  public static readonly AUDIO_EFFECT_ATTACK_RIFLE = 7;
  public static readonly AUDIO_EFFECT_ATTACK_UZI = 8;
  public static readonly AUDIO_EFFECT_ATTACK_FIST = 9;
  public static readonly AUDIO_EFFECT_ATTACK_SWORD = 10;
  public static readonly AUDIO_EFFECT_WALKING = [11, 12, 13, 14, 15];
  public static readonly AUDIO_EFFECT_DEATH = [16, 17];
  public static readonly AUDIO_EFFECT_EXPLOSION = 18;
  public static readonly AUDIO_EFFECT_COLLISION_BARREL = 19;

  public static readonly DURATION_STUN = 0.4;
  public static readonly DURATION_FOOT = 0.4;
  public static readonly DURATION_ATTACK = 0.4;
}
