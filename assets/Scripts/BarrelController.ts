import { Consts } from "./Consts"; 
import { ShakeEffect } from "./ShakeEffect";

const {
  ccclass,
  property
} = cc._decorator;

@ccclass
export default class BarrelController extends cc.Component {
  public anim = null;
  public bouncing: boolean = false;
  public exploding: boolean = false;
  public collisionCoolTime: number = 0;
	public god: cc.Component = null;

  start() {
		this.god = cc.find("God").getComponent("God");
    this.anim = this.getComponent(cc.Animation);
  }

  update(dt) { this.collisionCoolTime += dt; }

  onBeginContact(contact, self, other) {
    if (this.collisionCoolTime < 0.6) return;
    if (
      this.bouncing ||
      (other.node.name == "Western_Barrel3" && other.node.getComponent("BarrelController").bouncing)
    ) return;
    if (other.node.name != "stickman fighter") {
      cc.audioEngine.playEffect(this.god.audioClips[Consts.AUDIO_EFFECT_COLLISION_BARREL]);
      this.collisionCoolTime = 0;
    }
  }

  onCollisionEnter(other, self) {
		if (!this.bouncing && (
        other.tag == Consts.TAG_BULLET ||
        (
          other.node.name == "Western_Barrel3" &&
          other.node.getComponent("BarrelController").exploding
        )
    )) {
			this.bouncing = true;
      let action = cc.sequence(
        cc.scaleTo(0.2, 0.8, 1.2),
        cc.scaleTo(0.4, 1, 1),
        cc.scaleTo(0.2, 1.2, 0.8),
        cc.scaleTo(0.4, 1, 1),
        cc.scaleTo(0.2, 0.8, 1.2),
        cc.scaleTo(0.4, 1, 1),
        cc.scaleTo(0.2, 1.2, 0.8),
        cc.scaleTo(0.4, 1, 1),
        cc.callFunc(function(t) { this.exploding = true; } .bind(this), this), 
        cc.scaleTo(0.008, 3, 3),
        cc.callFunc(function(t) {
          cc.audioEngine.playEffect(this.god.audioClips[Consts.AUDIO_EFFECT_EXPLOSION]);
          this.anim.play("explo1");
          this.anim.on("finished", function() { this.node.destroy(); } .bind(this), this);
          this.shakeCamera();
        }, this);
      );
      this.node.runAction(action);
      if (other.tag == Consts.TAG_BULLET) other.node.destroy();
    }
  }

  onPreSolve(contact, self, other) {
    if (this.exploding && other.node.name == "stickman fighter") {
      let nrm = contact.getWorldManifold().normal;
      let vec = cc.v2(nrm.x * -1000, nrm.y * -1000);
      other.node.getComponent(cc.RigidBody).applyForceToCenter(vec);
      other.node.getComponent("PlayerController").hurt(0.03);
    }
  }

  shakeCamera() { cc.find("Canvas/Main Camera").addComponent("ShakeEffect"); }
}

