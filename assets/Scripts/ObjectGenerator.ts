const {
  ccclass,
  property
} = cc._decorator;

@ccclass
export default class ObjectGenerator extends cc.Component {
  @property({ type: [cc.Prefab] }) gifts: cc.Prefab[] = [];
  @property({ type: [cc.Prefab] }) snows: cc.Prefab[] = [];
  @property(cc.Prefab) barrel: cc.Prefab = null;
  @property(cc.Prefab) block: cc.Prefab = null;

	onLoad() { cc.director.getPhysicsManager().enabled = true; } 

  start() {
    let isMap2 = cc.find("Canvas/BG_Egypt");
    let isMap3 = this.block;

		this.schedule(() {
			let gift = cc.instantiate(this.gifts[Math.floor(Math.random() * this.gifts.length)]);
			this.node.addChild(gift);
			let x = 100 + Math.random() * 750;
			let y = 500 + Math.random() * 100;
      if (isMap2) {
        x = 94 + Math.random() * (850 - 94);
        y = 500 + Math.random() * (650 - 500);
      }
      if (isMap3) {
        x = 100 + Math.random() * (850 - 100);
        y = 500 + Math.random() * (600 - 500);
      }
			gift.position = cc.v2(x, y);
		}, 5, 1000000, 0); // interval, repeat, delay

    if (isMap2) {
      this.schedule(() {
        let barrel = cc.instantiate(this.barrel);
        this.node.addChild(barrel);
        let x = 94 + Math.random() * (850 - 94);
        let y = 500 + Math.random() * (650 - 500);
        barrel.position = cc.v2(x, y);
      }, 15, 1000000, 0); // interval, repeat, delay
    }

    if (isMap3) {
      this.schedule(() {
        let block = cc.instantiate(this.block);
        let that = this;
        setTimeout(function() {
          if (that.node == null) return;
          that.node.addChild(block);
          let x = 1015;
          let y = Math.random() < 0.5 ? 285 : 220;
          block.position = cc.v2(x, y);
          block.getComponent(cc.RigidBody).linearVelocity = cc.v2(-400, 0);
          setTimeout(function() { block.destroy(); }, 6000);
        }, Math.random() * 3000);
      }, 2, 1000000, 0); // interval, repeat, delay
    }

    if (this.snows.length) {
      this.schedule(() {
        let snow = cc.instantiate(this.snows[Math.floor(Math.random() * this.snows.length)]);
        this.node.addChild(snow);
        let x = Math.random() * 950;
        let y = 650;
        snow.position = cc.v2(x, y);
        setTimeout(function () { snow.destroy(); }.bind(this), 2000);
      }, 0.1, 1000000, 0); // interval, repeat, delay
    }
	}

	// update (dt) {}
}
