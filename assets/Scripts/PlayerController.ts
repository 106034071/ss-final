import { Consts } from "./Consts"; 

const {
  ccclass,
  property
} = cc._decorator;

@ccclass
export default class PlayerController extends cc.Component {
  @property(cc.Prefab) bullet1: cc.Prefab = null;

	public god: cc.Component = null;
	public isPlayer1: null;

  public playerSpeed: number = 250;

  public skeleton = null;
  public prevAnim: string = "";
  public sword = null;
  public fist = null;

  public mode: number = 1;
  public onGround: boolean = true;
  public jumping: boolean = false;
	public dying: boolean = false;
	public paralyzed: boolean = false;
  public attacking: boolean = false;
  public bulletCoolTime: number = 0;
  public idleTime: number = 0;
  public footCoolTime: number = 0;
  public attackReleased: boolean = true;

  public upDown: boolean = false;
  public leftDown: boolean = false;
  public downDown: boolean = false;
  public rightDown: boolean = false;
  public aDown: boolean = false;
  public bDown: boolean = false;
  public cDown: boolean = false;

  onLoad() {
    cc.director.getPhysicsManager().gravity = cc.v2(0, -2000);
  }

  start() {
		this.god = cc.find("God").getComponent("God");
    this.skeleton = this.getComponent(sp.Skeleton);
    this.sword = cc.find(
      "ATTACHED_NODE_TREE/ATTACHED_NODE:root/ATTACHED_NODE:hip/ATTACHED_NODE:chest" +
      "/ATTACHED_NODE:arm_L/ATTACHED_NODE:arm_L2/ATTACHED_NODE:weapon",
      this.node
    );
    this.fist = cc.find(
      "ATTACHED_NODE_TREE/ATTACHED_NODE:root/ATTACHED_NODE:hip/ATTACHED_NODE:chest" +
      "/ATTACHED_NODE:arm_L/ATTACHED_NODE:arm_L2",
      this.node
    );
  }

  update(dt) {
    if (this.god.transitioning) return;
		this.listenKeys();
    this.bulletCoolTime += dt;
    this.idleTime += dt;
    this.footCoolTime += dt;
    if (this.node.y < -100) {
      let r = Math.random() < 0.5 ? 0 : 1;
      this.playAnim(1, r ? "/die A" : "/die B", 0, []);
      cc.audioEngine.playEffect(this.god.audioClips[Consts.AUDIO_EFFECT_DEATH[r]);
      this.hurt(0.25);
      if (!this.dying) {
        let n = cc.director.getScene().name;
        if (this.isPlayer1) {
          this.node.position =
            n == "map1" ? cc.v2(200, 133) : n == "map2" ? cc.v2(253, 210) : cc.v2(190, 160);
        } else {
          this.node.position =
            n == "map1" ? cc.v2(800, 133) : n == "map2" ? cc.v2(835, 304) : cc.v2(400, 160);
        }
      }
      return;
    }
		if (this.dying || (this.paralyzed && this.idleTime < Consts.DURATION_STUN)) return;
    this.paralyzed = false;
    this.idleTime = 0;

    let dir = this.leftDown ? -1 : this.rightDown ? +1 : 0;
		if (dir) this.node.scaleX = (dir > 0) ? +0.08 : -0.08;
		this.node.x += this.playerSpeed * dir * dt;
		this.getComponent(cc.RigidBody).applyForceToCenter(new cc.Vec2(0.005, 0));

    this.attacking = false;
    if (this.upDown && !this.jumping) {
      this.jumping = true;
      let v = this.getComponent(cc.RigidBody).linearVelocity;
      this.getComponent(cc.RigidBody).linearVelocity = cc.v2(v.x, 0);
      this.getComponent(cc.RigidBody).applyForceToCenter(new cc.Vec2(0, 50000));
      this.playAnim(1, "/jump A", 0, []);
      this.skeleton.addAnimation(1, Consts.MODES[this.mode] + "/jump B", 0);
    } else if (!this.onGround && this.getComponent(cc.RigidBody).linearVelocity.y < 0) {
      this.playAnim(1, "/jump C", 0, []);
    } else if (this.onGround) {
			if (this.aDown) {
        this.attacking = true;
				if (dir && this.mode > 2) {
					let atks = [ "/", "/jab single", "/slash", "/run shoot", "/run shoot", "/run shoot" ];
					let track = this.playAnim(1, atks[this.mode], 1, ["/jump D", "/run stop"], false);
          if (this.footCoolTime >= Consts.DURATION_FOOT) {
            this.footCoolTime = 0;
            let r = Math.floor(Math.random() * 5);
            cc.audioEngine.playEffect(this.god.audioClips[Consts.AUDIO_EFFECT_WALKING[r]]);
          }
				} else {
					let atks = [ "/", "/jab single", "/slash", "/shoot", "/shoot", "/shoot" ];
					let track = this.playAnim(1, atks[this.mode], 0, ["/jump D", "/run stop"], false);
					this.node.x -= this.playerSpeed * dir * dt; // cancel movement
				}

        if (this.mode == 1) {
          if (this.attackReleased) {
            cc.audioEngine.playEffect(this.god.audioClips[Consts.AUDIO_EFFECT_ATTACK_FIST]);
          }
        } else if (this.mode == 2) {
          if (this.attackReleased) {
            cc.audioEngine.playEffect(this.god.audioClips[Consts.AUDIO_EFFECT_ATTACK_SWORD]);
          }
        } else {
          if (this.bulletCoolTime >= Consts.MODE_BULLET_CD[this.mode]) {
            cc.audioEngine.playEffect(this.god.audioClips[
              [
                Consts.AUDIO_EFFECT_ATTACK_PISTOL,
                Consts.AUDIO_EFFECT_ATTACK_UZI, 
                Consts.AUDIO_EFFECT_ATTACK_RIFLE
              ][this.mode - 3]
            ]);
            this.bulletCoolTime = 0;
            let bullet = cc.instantiate(this.bullet1);
            bullet.scaleX = this.node.scaleX;
            bullet.parent = cc.director.getScene();
            let pos = this.sword.convertToWorldSpaceAR(cc.v2(0, 0));
            bullet.setPosition(pos.x + (bullet.scaleX < 0 ? -50 : +50), pos.y);
            bullet.getComponent(cc.RigidBody).linearVelocity = cc.v2(
              (bullet.scaleX < 0 ? -1 : +1) * Consts.MODE_BULLET_SPEED[this.mode], 0
            );
          }
        }
			} else if (dir) {
        this.playAnim(1, "/run", 1, ["/jump D", "/run stop"], false);
        if (this.footCoolTime >= Consts.DURATION_FOOT) {
          this.footCoolTime = 0;
          let r = Math.floor(Math.random() * 5);
          cc.audioEngine.playEffect(this.god.audioClips[Consts.AUDIO_EFFECT_WALKING[r]]);
        }
      } else {
        this.playAnim(1, "/idle active", 1, ["/jump D", "/run stop"], false);
      }
    }

    this.attackReleased = !this.attacking;
  }

  playAnim(trackIndex, animNext, loop, waitList, forceWrite = true) {
		animNext = Consts.MODES[this.mode] + animNext;
    if (forceWrite || animNext != this.prevAnim) {
      if (waitList.includes("/" + this.prevAnim.split("/")[1])) {
        return this.skeleton.addAnimation(trackIndex, this.prevAnim = animNext, loop);
      } else {
        return this.skeleton.setAnimation(trackIndex, this.prevAnim = animNext, loop);
      }
    }
    return false;
  }

  onCollisionEnter(other, self) {
    let p = (this.isPlayer1 ? this.god.player2 : this.god.player1).getComponent("PlayerController");
    if (p.attacking) {
      if (other.node == p.sword) {
        if (p.mode == Consts.MODE_SWORD) {
          this.hurt(0.10);
        }
      } else if (other.node == p.fist) {
        if (p.mode == Consts.MODE_FIST) {
          this.hurt(0.05);
        }
      }
    }
    if (other.tag == Consts.TAG_BULLET) {
      other.node.destroy();
      this.hurt(0.05);
    }
  }

  onBeginContact(contact, self, other) {
    if (other.tag == Consts.TAG_GROUND) {
      this.onGround = true;
      let nrm = contact.getWorldManifold().normal;
      if (Math.abs(nrm.x)**2 + Math.abs(nrm.y - 1)**2 < 0.01) return;
      if (this.jumping) {
        this.jumping = false;
        this.playAnim(1, "/jump D", 0, []);
        cc.audioEngine.playEffect(this.god.audioClips[Consts.AUDIO_EFFECT_LAND]);
      }
    }
  }

  onEndContact(contact, self, other) {
    if (other.tag == Consts.TAG_GROUND &&
        !(["stickman fighter", "Western_Barrel3"].includes(other.node.name))
       ) {
      this.onGround = false;
    }
  }

	suicide() {
    if (this.god.transitioning) return;
		if (this.dying) return;
		this.dying = true;
    if (this.isPlayer1) cc.find("blood_p1").getComponent(cc.Sprite).fillStart = 0;
    else cc.find("blood_p2").getComponent(cc.Sprite).fillStart = 1;
    let r = Math.random() < 0.5 ? 0 : 1;
		this.playAnim(1, r ? "/die A" : "/die B", 0, []);
    cc.audioEngine.playEffect(this.god.audioClips[Consts.AUDIO_EFFECT_DEATH[r]);
    this.god.nextMap(2);
	}

	revive() { this.dying = false; }

	hurt(dmg) {
    if (this.isPlayer1) {
      let hp = cc.find("blood_p1").getComponent(cc.Sprite).fillStart;
      if ((hp -= dmg) <= 0.005) return this.suicide();
      cc.find("blood_p1").getComponent(cc.Sprite).fillStart = hp;
    } else {
      let hp = 1 - cc.find("blood_p2").getComponent(cc.Sprite).fillStart;
      if ((hp -= dmg) <= 0.005) return this.suicide();
      cc.find("blood_p2").getComponent(cc.Sprite).fillStart = 1 - hp;
    }
    if (this.paralyzed) return; // TODO: Flicker for invincibility?
		this.paralyzed = true;
    if (!this.dying) this.playAnim(1, "/hurt center", 0, []);
	}

	listenKeys() {
		this.upDown = this.isPlayer1 ? this.god.wDown : this.god.arrowUpDown;
		this.leftDown = this.isPlayer1 ? this.god.aDown : this.god.arrowLeftDown;
		this.downDown = this.isPlayer1 ? this.god.sDown : this.god.arrowDownDown;
		this.rightDown = this.isPlayer1 ? this.god.dDown : this.god.arrowRightDown;
		this.aDown = this.isPlayer1 ? this.god.vDown : this.god.iDown;
		this.bDown = this.isPlayer1 ? this.god.bDown : this.god.oDown;
		this.cDown = this.isPlayer1 ? this.god.nDown : this.god.pDown;
	}

  onPreSolve(contact, self, other) {
    if (other.node.name == "conveyor") {
      let v = this.getComponent(cc.RigidBody).linearVelocity;
      this.getComponent(cc.RigidBody).linearVelocity = cc.v2(-120, v.y);
    }
  }
}

