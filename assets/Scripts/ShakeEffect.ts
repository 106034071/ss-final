const {
  ccclass,
  property
} = cc._decorator;

@ccclass
export default class ShakeEffect extends cc.Component {
  @property
  shakeAmount: number = 10;
  @property
  shakeDuration: number = 1.5;

  public pos0: cc.v3 = null;

  randomSpherePoint(r) {
    let u = Math.random();
    let v = Math.random();
    let theta = 2 * Math.PI * u;
    let phi = Math.acos(2 * v - 1);
    let x = r * Math.sin(phi) * Math.cos(theta);
    let y = r * Math.sin(phi) * Math.sin(theta);
    let z = r * Math.cos(phi);
    return cc.v3(x, y, z);
  }

  start() {
    this.pos0 = cc.v3(0, 0, 554.2562584220408); // this.node.position;
  }

  update(dt) {
    if (this.shakeDuration > 0) {
      this.node.position = this.pos0.add(this.randomSpherePoint(this.shakeAmount));
      this.shakeDuration -= dt;
    } else {
      this.node.position = this.pos0;
    }
  }
}
